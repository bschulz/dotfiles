#!/usr/bin/env bash


# Environment Vars

# Android
export JAVA_HOME=`/usr/libexec/java_home -v '1.8*'`
export ANDROID_HOME="/usr/local/share/android-sdk"
export ANDROID_SDK_ROOT="/usr/local/share/android-sdk"
export ANDROID_NDK_HOME="/usr/local/share/android-ndk"

# Gradle
export GRADLE_HOME=/usr/local/bin/gradle

# Groovy
export GROOVY_HOME=/usr/local/opt/groovy/libexec


# Configure Java

jenv add /Library/Java/JavaVirtualMachines/jdk1.8.0_131.jdk/Contents/Home/




