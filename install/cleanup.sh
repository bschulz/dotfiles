#!/usr/bin/env bash

# Remove Android Studio's SDK and JDK
rm -rf ~/Library/Android
